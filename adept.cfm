<cfif IsDebugMode()>
</td></td></td></th></th></th></tr></tr></tr></table></table></table></a></abbrev></acronym></address></applet></au></b></banner></big></blink></blockquote></bq></caption></center></cite></code></comment></del></dfn></dir></div></div></dl></em></fig></fn></font></form></frame></frameset></h1></h2></h3></h4></h5></h6></head></i></ins></kbd></listing></map></marquee></menu></multicol></nobr></noframes></noscript></note></ol></p></param></person></plaintext></pre></q></s></samp></script></select></small></strike></strong></sub></sup></table></td></textarea></th></title></tr></tt></u></ul></var></wbr></xmp>
<style type="text/css">
.cfdebug { color: #000000; background-color: #FFFFFF; font: 10pt Arial; }
.cfdebuglge {	color: #000000;	background-color: #FFFFFF; font: 11pt Arial; }
.cfdebug_td_heading { color: #000000; background-color: #ECDDC6; font: 10pt Arial; font-weight: bold; }
.cfdebug_td_data { color: #000000; background-color: #FFFFFF; font: 8pt Arial; vertical-align: top; }

.ScopeHeading { color: #FFFFFF; background-color: #c0392b; font: 11pt Arial; font-weight: bold; }
.ScopeVariableName { color: #c0392b; background-color: #FFFFFF; font: 10pt Arial; font-weight: bold; }
.ScopeVariableValue { color: #000000; background-color: #FFFFFF; font: 10pt Arial; }

table.cfdebug tr.rowLight td { background: #FFF;}
table.cfdebug tr.rowDark td { background: rgb(247, 244, 244);}

table.cfdebug td { padding: 4px; }
table.cfdebug td.queryname { font-weight: bold; }
.template {	color: #000000;	background-color: #FFFFFF; font: 10pt Arial }
.template_slow td {	color: #c0392b;	background-color: #FFFFFF; font: 10pt Arial }

td.executiontime { text-align: right; font: 10pt Arial; }
.query_slow td.executiontime, .template_slow td.executiontime { border-top: 6px solid #c0392b; color: #c0392b; font-weight: bold; }
.query_warn td.executiontime, .template_warn td.executiontime { border-top: 6px solid #e67e22; color: #e67e22; font-weight: bold; }
.query_cached td.executiontime { border-top: 6px solid #27ae60; color: #27ae60; font-weight: bold; }

span.template_slow { color: #c0392b; }
span.template_warn { color: #e67e22; }
span.cached { color: #27ae60; }

table.cfdebug table.parameters th { background: gray; color: #FFF; }

A.cfdebuglink { color: #00009C;	background-color: #FFFFFF; text-decoration: none; }
A.cfdebuglink:hover { color: #9C0000;	background-color: #FFFFFF; text-decoration: underline; }

.divAdeptDebug
{
  display: none;
	background: #FFF;
}

a.cfdebugtogglelink
{
  font-size: 10px !important;
  margin: 2px;
  color: #999;
}

.ad-left {text-align: left;}
.ad-center {text-align: center;}
.ad-right {text-align: right;}
</style>

<cfset ExecTimeWarn = 1000>
<cfset ExecTimeSlow = 3000>

<script>
	function fnShowHideDebug(id)
	{
		var objAdeptDebugDiv = document.getElementById(id);

		if (objAdeptDebugDiv.style.display == 'none' || objAdeptDebugDiv.style.display == '')
		{
				objAdeptDebugDiv.style.display = 'block';
		}
		else
		{
				objAdeptDebugDiv.style.display = 'none';
		}
	}
</script>

<cfset AdeptDebugDivName = 'divAdeptDebug#CreateUUID()#'>

<cfoutput>
<table width="100%">
	<tbody>
		<tr>
			<td class="ad-left"><a href="javascript:fnShowHideDebug('#AdeptDebugDivName#');" class="cfdebugtogglelink">Toggle Debug</a></td>
			<td class="ad-center"><a href="javascript:fnShowHideDebug('#AdeptDebugDivName#');" class="cfdebugtogglelink">Toggle Debug</a></td>
			<td class="ad-right"><a href="javascript:fnShowHideDebug('#AdeptDebugDivName#');" class="cfdebugtogglelink">Toggle Debug</a></td>
		</tr>
	</tbody>
</table>
</cfoutput>

<div id="<cfoutput>#AdeptDebugDivName#</cfoutput>" class="divAdeptDebug">
<HR>

<TABLE WIDTH="800" BORDER="0" CELLPADDING="0" CELLSPACING="0">
<TR>
	<TD STYLE="padding-left: 8px;">

<cfflush>
<cfsilent>
<cfset startTime = getTickCount()>
<cfsetting enablecfoutputonly="false">
<cfscript>
	setEncoding("FORM", "UTF-8");
	setEncoding("URL", "UTF-8");
</cfscript>


	<!--- Localized strings --->
	<cfset undefined = "">

	<!--- Use the debugging service to check options --->
	<cftry>
		<cfobject action="CREATE" type="JAVA" class="coldfusion.server.ServiceFactory" name="factory">
		<cfset cfdebugger = factory.getDebuggingService()>
		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- Load the debugging service's event table --->
	<cfset qEvents = cfdebugger.getDebugger().getData()>

	<!--- Produce the filtered event queries --->
	<!--- EVENT: Templates --->
	<cftry>
		<cfquery dbType="query" name="qTemplates" debug="false">
			SELECT template, parent, Sum(endTime - StartTime) AS ExecutionTime
			FROM qEvents
			WHERE type = 'Template'
			GROUP BY template, parent
			ORDER BY ExecutionTime DESC
		</cfquery>

		<cfquery dbType="query" name="qTemplateSummary" debug="false">
			SELECT
				SUM(ExecutionTime) As TotalExecutionTime,
				AVG(ExecutionTime) As AvgExecutionTime
			FROM
				qTemplates;
		</cfquery>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>


	<!--- EVENT: SQL Queries --->
	<cftry>
		<cfquery dbType="query" name="qQueries" debug="false">
			SELECT *, (endTime - startTime) AS executionTime
			FROM qEvents
			WHERE type = 'SqlQuery'
		</cfquery>

		<cfquery dbType="query" name="qQuerySummary" debug="false">
			SELECT
				CachedQuery,
				COUNT(*) As QueryCount,
				SUM(ExecutionTime) As TotalExecutionTime,
				AVG(ExecutionTime) As AvgExecutionTime
			FROM
				qQueries
			GROUP BY
				CachedQuery;
		</cfquery>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- EVENT: Object Queries --->
	<cftry>
		<cfquery dbType="query" name="cfdebug_cfoql" debug="false">
			SELECT *, (endTime - startTime) AS executionTime
			FROM qEvents
			WHERE type = 'ObjectQuery'
		</cfquery>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- EVENT: Stored Procedures --->
	<cftry>
		<cfquery dbType="query" name="qStoredProcs" debug="false">
			SELECT *, (endTime - startTime) AS executionTime
			FROM qEvents
			WHERE type = 'StoredProcedure'
		</cfquery>

		<cfquery dbType="query" name="qSPSummary" debug="false">
			SELECT
				CachedQuery,
				COUNT(*) As QueryCount,
				SUM(ExecutionTime) As TotalExecutionTime,
				AVG(ExecutionTime) As AvgExecutionTime
			FROM
				qStoredProcs
			GROUP BY
				CachedQuery;
		</cfquery>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- EVENT: Trace Points --->
	<cftry>
	<cfquery dbType="query" name="cfdebug_trace" debug="false">
	SELECT *
	FROM qEvents
	WHERE type = 'Trace'
	</cfquery>
		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- EVENT: Locking Warning Points --->
	<cftry>
	<cfquery dbType="query" name="cfdebug_lock" debug="false">
	SELECT *
	FROM qEvents
	WHERE type = 'LockWarning'
	</cfquery>
		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<!--- EVENT: Exceptions --->
	<cftry>
	<cfquery dbType="query" name="cfdebug_ex" debug="false">
	SELECT *
	FROM qEvents
	WHERE type = 'Exception'
	</cfquery>
		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>

	<cfquery dbType="query" name="qTemplates_summary" debug="false">
		SELECT  template, Sum(endTime - startTime) AS totalExecutionTime, count(template) AS instances
		FROM qEvents
		WHERE type = 'Template'
		group by template
		order by totalExecutionTime DESC
	</cfquery>

	<!--- Establish Section Display Flags --->
	<cfparam name="bFoundExecution" default="false" type="boolean">
	<cfparam name="bFoundTemplates" default="false" type="boolean">
	<cfparam name="bFoundExceptions" default="false" type="boolean">
	<cfparam name="bFoundSQLQueries" default="false" type="boolean">
	<cfparam name="bFoundObjectQueries" default="false" type="boolean">
	<cfparam name="bFoundStoredProc" default="false" type="boolean">
	<cfparam name="bFoundTrace" default="false" type="boolean">
	<cfparam name="bFoundLocking" default="false" type="boolean">
	<cfparam name="bFoundScopeVars" default="false" type="boolean">

	<cftry>
		<cfscript>
        // no longer doing template query at the top since we have tree and summary mode
		bFoundTemplates = cfdebugger.check("Template");

		if (IsDefined("cfdebug_ex") AND cfdebug_ex.recordCount GT 0) { bFoundExceptions = true; }
		else { bFoundExceptions = false; }

		if (IsDefined("qQueries") AND qQueries.RecordCount GT 0) { bFoundSQLQueries = true; }
		else { bFoundSQLQueries = false; }

		if (IsDefined("cfdebug_cfoql") AND cfdebug_cfoql.RecordCount GT 0) { bFoundObjectQueries = true; }
		else { bFoundObjectQueries = false; }

		if (IsDefined("qStoredProcs") AND qStoredProcs.RecordCount GT 0) { bFoundStoredProc = true; }
		else { bFoundStoredProc = false; }

		if (IsDefined("cfdebug_trace") AND cfdebug_trace.recordCount GT 0) { bFoundTrace = true; }
		else { bFoundTrace = false; }

		if (IsDefined("cfdebug_lock") AND cfdebug_lock.recordCount GT 0) { bFoundLocking = true; }
		else { bFoundLocking = false; }

		if (IsDefined("cfdebugger") AND cfdebugger.check("Variables")) { bFoundScopeVars = true; }
		else { bFoundScopeVars = false; }
		</cfscript>
		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>
</cfsilent>

<cfoutput>
<cftry>
	<br><br>

	<table border="1" BORDERCOLOR="##7E695D" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
	<tr>
		<td class="cfdebug_td_heading" align="center" COLSPAN="2">#server.coldfusion.productname# #server.coldfusion.productlevel#</td>
	</tr>
	<tr>
		<td class="cfdebug" nowrap>Template </td>
		<td class="cfdebug">#CGI.Script_Name#</td>
	</tr>
	<tr>
		<td class="cfdebug" nowrap>Time Stamp </td>
		<td class="cfdebug">#DateFormat(Now(), 'mmmm d, yyyy')# #TimeFormat(Now(), 'h:mm:ss tt')#</td>
	</tr>
	<tr>
		<td class="cfdebug" nowrap>Locale </td>
		<td class="cfdebug">#GetLocale()#</td>
	</tr>
	<tr>
		<td class="cfdebug" nowrap>User Agent </td>
		<td class="cfdebug">#CGI.HTTP_USER_AGENT#</td>
	</tr>
	<tr>
		<td class="cfdebug" nowrap>Remote IP </td>
		<td class="cfdebug">#CGI.REMOTE_ADDR#</td>
	</tr>
	</table>

	<BR>
	<BR>

	<table border="1" BORDERCOLOR="##7E695D" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
		<tr>
			<td class="cfdebug_td_heading" align="center" COLSPAN="4">Summary</td>
		</tr>
		<tr>
			<td></td>
			<td>Count</td>
			<td>Avg</td>
			<td>Total</td>
		</tr>
		<tr>
			<td class="cfdebug" nowrap>Templates</td>
			<td class="cfdebug">#qTemplates_summary.RecordCount#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qTemplateSummary.AvgExecutionTime)#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qTemplateSummary.TotalExecutionTime)#</td>
		</tr>
		<tr>
			<td class="cfdebug" nowrap>Queries</td>
			<td class="cfdebug">#qQueries.RecordCount#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qQuerySummary.AvgExecutionTime)#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qQuerySummary.TotalExecutionTime)#</td>
		</tr>
		<tr>
			<td class="cfdebug" nowrap>Stored Procs</td>
			<td class="cfdebug">#qStoredProcs.RecordCount#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qSPSummary.AvgExecutionTime)#</td>
			<td class="cfdebug">#ExecutionTimeFormat(qSPSummary.TotalExecutionTime)#</td>
		</tr>
	</table>

	<br><br>

	<cfcatch type="Any">
		<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
		<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
	</cfcatch>
</cftry>
</cfoutput>

<!--- Template Stack and Executions Times --->
<cfif bFoundTemplates>
	<br><br>
	<h3>Templates</h3>

	<!--- Total Execution Time of all top level pages --->
	<cfquery dbType="query" name="cfdebug_execution" debug="false">
			SELECT (endTime - startTime) AS executionTime
			FROM qEvents
			WHERE type = 'ExecutionTime'
	</cfquery>

	<cfquery dbType="query" name="cfdebug_top_level_execution_sum" debug="false">
		SELECT sum(endTime - startTime) AS executionTime
		FROM qEvents
		WHERE type = 'Template' AND parent = ''
	</cfquery>

	<!--- File not found will not produce any records when looking for top level pages --->
	<cfif cfdebug_top_level_execution_sum.recordCount GT 0>
			<cfset time_other = Max(cfdebug_execution.executionTime - cfdebug_top_level_execution_sum.executionTime, 0)>
			<p class="cfdebug"><hr/><b class="cfdebuglge"><a name="cfdebug_execution">Execution Time</a></b></p>
			<a name="qTemplates">

			<cfif cfdebugger.settings.template_mode EQ "tree">
					<cfset a = arrayNew(1)>
					<cfloop query="qEvents">
							<cfscript>
									// only want templates, IMQ of SELECT * ...where type = 'template' will result
									// in cannot convert the value "" to a boolean for cachedquery column
									// SELECT stacktrace will result in Query Of Queries runtime error.
									// Failed to get meta_data for columnqEvents.stacktrace .
									// Was told I need to define meta data for debugging event table similar to <cfldap>
									if( qEvents.type eq "template" ) {
											st = structNew();
											st.StackTrace = qEvents.stackTrace;
											st.template = qEvents.template;
											st.startTime = qEvents.starttime;
											st.endTime = qEvents.endtime;
											st.parent =  qEvents.parent;
											st.line =  qEvents.line;

											arrayAppend(a, st);
									}
							</cfscript>
					</cfloop>
					<cfset qTree = queryNew("template,templateId,parentId,duration,line")>
					<cfloop index="i" from="1" to="#arrayLen(a)#">
							<cfset childidList = "">
							<cfset parentidList = "">
							<cfloop index="x" from="#arrayLen(a[i].stacktrace.tagcontext)#" to="1" step="-1">
									<cfscript>
											if( a[i].stacktrace.tagcontext[x].id NEQ "CF_INDEX" ) {
													// keep appending the line number from the template stack to form a unique id
													childIdList = listAppend(childIdList, a[i].stacktrace.tagcontext[x].line);
													if( x eq 1 ) {
															parentIdList = listAppend(parentIdList, a[i].stacktrace.tagcontext[x].template);
													} else {
															parentIdList = listAppend(parentIdList, a[i].stacktrace.tagcontext[x].line);
													}
											}
									</cfscript>
							</cfloop>

							<cfscript>
									// template is the last part of the unique id...12,5,17,c:\wwwroot\foo.cfm
									// if we don't remove the "CFC[" prefix, then the parentId and childId relationship
									// will be all wrong
									startToken = "CFC[ ";
									endToken = " | ";
									thisTemplate = a[i].template;
									startTokenIndex = FindNoCase(startToken, thisTemplate, 1);
									if( startTokenIndex NEQ 0 ) {
											endTokenIndex = FindNoCase(endToken, thisTemplate, startTokenIndex);
											thisTemplate = Trim(Mid(thisTemplate,Len(startToken),endTokenIndex-Len(startToken)));
									}
									childIdList = listAppend(childIdList, thisTemplate);

									queryAddRow(qTree);
									querySetCell(qTree, "template", a[i].template);
									querySetCell(qTree, "templateId", childIdList);
									querySetCell(qTree, "parentId", parentIdList);
									querySetCell(qTree, "duration", a[i].endtime - a[i].starttime);
									querySetCell(qTree, "line", a[i].line);
							</cfscript>
					</cfloop>

					<cfset stTree = structNew()>
					<cfloop query="qTree">
							<cfscript>
							// empty parent assumed to be top level with the exception of application.cfm
							if( len(trim(parentId)) eq 0 ){
									parentId = 0;
							}
									stTree[parentId] = structNew();
									stTree[parentId].templateId = qTree.templateId;
									stTree[parentId].template = qTree.template;
									stTree[parentId].duration = qTree.duration;
									stTree[parentId].line = qTree.line;
									stTree[parentId].children = arrayNew(1);
							</cfscript>
					</cfloop>
					<cfloop query="qTree">
							<cfscript>
									stTree[templateId] = structNew();
									stTree[templateId].templateId = qTree.templateId;
									stTree[templateId].template = qTree.template;
									stTree[templateId].duration = qTree.duration;
									stTree[templateId].line = qTree.line;
									stTree[templateId].children = arrayNew(1);
							</cfscript>
					</cfloop>
					<cfloop query="qTree">
							<cfscript>
									arrayAppend(stTree[parentId].children, stTree[templateId]);
							</cfscript>
					</cfloop>

					<cfquery dbType="query" name="topNodes" debug="false">
							SELECT parentId, template
							FROM qTree
							WHERE parentId = ''
					</cfquery>

					<cfoutput query="topNodes">
							#drawTree(stTree,-1,topNodes.template,cfdebugger.settings.template_highlight_minimum)#
					</cfoutput>
					<cfoutput><p class="template">
							(#time_other# ms) STARTUP, PARSING, COMPILING, LOADING, &amp; SHUTDOWN<br />
							(#cfdebug_execution.executionTime# ms) TOTAL EXECUTION TIME<br />
							<font color="red"><span class="template_slow">red = over #cfdebugger.settings.template_highlight_minimum# ms execution time</span></font>
							</p></cfoutput>
			<cfelse>

				<BR>
				<BR>

				<cftry>
							<cfoutput>
							<table border="1" BORDERCOLOR="##7E695D" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
							<tr>
								<td class="cfdebug_td_heading" align="center">Request</td>
								<td class="cfdebug_td_heading" align="center">Total Time</td>
								<td class="cfdebug_td_heading" align="center">Avg Time</td>
								<td class="cfdebug_td_heading" align="center">Cost</td>
								<td class="cfdebug_td_heading" align="center">Count</td>
								<td class="cfdebug_td_heading">Template</td>
							</tr>
							</cfoutput>

							<cftry>
									<cfoutput query="qTemplates_summary">
											<cfset templateOutput = template>
											<cfset templateAverageTime = Round(totalExecutionTime / instances)>

											<cfif template EQ ExpandPath(cgi.script_name)>
													<cfset templateOutput = "<b>" & template & "</b>">
											</cfif>

											<cfif templateAverageTime GTE ExecTimeSlow>
												<cfset ExecTimeClass = 'query_slow'>
											<cfelseif templateAverageTime GTE ExecTimeWarn>
												<cfset ExecTimeClass = 'query_warn'>
											<cfelse>
												<cfset ExecTimeClass = ''>
											</cfif>

											<cfset TemplateCost = (Max(totalExecutionTime, 0) / cfdebug_top_level_execution_sum.executiontime) * 100>

											<tr class="<cfif qTemplates_summary.currentRow mod 2>rowLight<cfelse>rowDark</cfif> #ExecTimeClass#">
													<td align="right" class="cfdebug_td_data" nowrap>#qTemplates_summary.currentrow#</td>
													<td align="right" class="cfdebug_td_data executiontime" nowrap>#ExecutionTimeFormat(totalExecutionTime)#</td>
													<td align="right" class="cfdebug_td_data executiontime" nowrap>#ExecutionTimeFormat(templateAverageTime)#</td>
													<td align="left" class="cfdebug_td_data" nowrap>#NumberFormat(TemplateCost, '0')#%</td>
													<td align="center" class="cfdebug_td_data" nowrap>#instances#</td>
													<td align="left" class="cfdebug_td_data" nowrap>#templateOutput#</td>
											</tr>
											</cfoutput>
								<cfcatch type="Any">
									<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
									<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
								</cfcatch>
							</cftry>
							<cfoutput>
							<tr>
								<td align="right" class="cfdebug_td_data" nowrap>&nbsp;</td>
								<td align="right" class="cfdebug_td_data" nowrap><i>#time_other# ms</i></td>
								<td colspan=3>&nbsp;</td>
								<td align="left" class="cfdebug_td_data"><i>STARTUP, PARSING, COMPILING, LOADING, &amp; SHUTDOWN</i></td>
							</tr>
							<tr>
								<td align="right" class="cfdebug_td_data" nowrap>&nbsp;</td>
								<td align="right" class="cfdebug_td_data" nowrap><i>#cfdebug_execution.executionTime# ms</i></td>
								<td colspan=3>&nbsp;</td>
								<td align="left" class="cfdebug_td_data"><i>TOTAL EXECUTION TIME</i></td>
							</tr>
							</table>
							<span class="template_slow">red = over #ExecTimeSlow#ms average execution time</span><br>
							<span class="template_warn">orange = over #ExecTimeWarn#ms average execution time</span><br>
							<span class="cached">green = cached</span>
							</a>
						</cfoutput>
				<cfcatch type="Any">
					<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
					<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
				</cfcatch>

				</cftry>
			</cfif> <!--- template_mode = summary--->
	<cfelse>
			<p class="cfdebug"><hr/><b class="cfdebuglge"><a name="cfdebug_execution">Execution Time</a></b></p>
			<a name="qTemplates">
			No top level page was found.
	</cfif> <!--- if top level templates are available --->
</cfif>

<!--- Exceptions --->
<cfif bFoundExceptions>
	<br><br>
	<h3>Exceptions</h3>

	<cftry>
		<cfoutput>
		<table border="1" BORDERCOLOR="##7E695D" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
		<tr>
			<td class="cfdebug_td_heading" align="center">Time</td>
			<td class="cfdebug_td_heading" align="center">Type</td>
			<td class="cfdebug_td_heading" align="center">Template</td>
			<td class="cfdebug_td_heading" align="center">Line</td>
			<td class="cfdebug_td_heading" align="center">Message</td>
			<td class="cfdebug_td_heading" align="center">Detail</td>
		</tr>
		<cfloop query="cfdebug_ex">
		<TR class="<cfif cfdebug_ex.currentRow mod 2>rowLight<cfelse>rowDark</cfif>">
				<td align="right" class="cfdebug_td_data" nowrap>#TimeFormat(cfdebug_ex.timestamp, "HH:mm:ss.SSS")#</td>
				<td align="left" class="cfdebug_td_data" nowrap>#cfdebug_ex.name#</td>
				<td align="center" class="cfdebug_td_data" nowrap>#cfdebug_ex.template#</td>
				<td align="right" class="cfdebug_td_data" nowrap>#cfdebug_ex.line#</td>
				<td align="right" class="cfdebug_td_data" nowrap>
					<cfif IsDefined("cfdebug_ex.message") AND Len(Trim(cfdebug_ex.message)) GT 0>
						#cfdebug_ex.message#
					</cfif>
				</td>
				<td align="right" class="cfdebug_td_data" nowrap>
						<CFSET stDebug = StructNew()>
						<CFSET stDebug = cfdebug_ex.stacktrace>
						#stDebug.Detail#
				</td>
		</TR>
		</cfloop>
		</TABLE>
		</cfoutput>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>
</cfif>

<!--- SQL Queries --->
<cfoutput>
<cfif bFoundSQLQueries>
	<cftry>
		<br><br>
		<h3>Queries (#qQueries.RecordCount#)</h3>

		<table border="1" bordercolor="##7e695d" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
			<tr>
				<td class="cfdebug_td_heading" align="center">Query</td>
				<td class="cfdebug_td_heading" align="center">Query Name</td>
				<td class="cfdebug_td_heading" align="center">Datasource</td>
				<td class="cfdebug_td_heading" align="center">Records</td>
				<td class="cfdebug_td_heading">Time</td>
				<td class="cfdebug_td_heading">Cost</td>
				<td class="cfdebug_td_heading">SQL</td>
				<td class="cfdebug_td_heading">Parameters</td>
			</tr>
			<cfloop query="qQueries">
				<cfif Max(qQueries.executionTime, 0) GTE ExecTimeSlow>
					<cfset ExecTimeClass = 'query_slow'>
				<cfelseif Max(qQueries.executionTime, 0) GTE ExecTimeWarn>
					<cfset ExecTimeClass = 'query_warn'>
				<cfelseif Val(qQueries.cachedquery)>
					<cfset ExecTimeClass = 'query_cached'>
				<cfelse>
					<cfset ExecTimeClass = ''>
				</cfif>

				<cfif Val(qQuerySummary.TotalExecutionTime) GT 0>
					<cfset QueryCost = (Max(qQueries.executionTime, 0) / qQuerySummary.TotalExecutionTime) * 100>
				<cfelse>
					<cfset QueryCost = 0>
				</cfif>

				<tr class="<cfif qQueries.currentRow mod 2>rowLight<cfelse>rowDark</cfif> #ExecTimeClass#">
					<td align="right" class="cfdebug_td_data" nowrap>#qQueries.CurrentRow#</td>
					<td align="right" class="cfdebug_td_data queryname" nowrap>#qQueries.name#</td>
					<td align="right" class="cfdebug_td_data" nowrap>#qQueries.datasource#</td>
					<td align="center" class="cfdebug_td_data" nowrap>
						<cfif IsDefined("qQueries.rowcount") AND IsNumeric(qQueries.rowcount)>
							#Max(qQueries.rowcount, 0)#
						<cfelseif IsDefined("qQueries.result.recordCount")>
							#qQueries.result.recordCount#
						</cfif>
					</td>
					<td align="left" class="cfdebug_td_data executiontime" nowrap>#Max(qQueries.executionTime, 0)# ms <CFIF qQueries.cachedquery>Cached Query</CFIF></td>
					<td align="left" class="cfdebug_td_data" nowrap>#NumberFormat(QueryCost, '0')#%</td>
					<td class="cfdebug_td_data" style="padding-right: 100px;" >
						#qQueries.body#
						<br><br>
						#qQueries.Template# at line #qQueries.Line#
					</td>
					<td>
						<cfif ArrayLen(qQueries.attributes)>
							<table border="1" cellpadding="2" cellspacing="2" class="parameters">
								<tr><th colspan="2" align="center">parameters</th></tr>
								<tr>
									<td><code><i>CFSQLType</i></code></td>
									<td><code><i>value</i></code></td>
								</tr>
								<cfloop index="x" from=1 to="#ArrayLen(qQueries.attributes)#">
									<cfset thisParam = #qQueries.attributes[qQueries.currentRow][x]#>

									<tr>
											<td><code><cfif StructKeyExists(thisParam, "sqlType")>#thisParam.sqlType#</cfif></code></td>
											<td><code><cfif StructKeyExists(thisParam, "value")>#thisParam.value#</cfif></code></td>
									</tr>
								</cfloop>
							</table>
						</cfif>
					</td>
				</tr>
			</cfloop>
		</table>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>
</cfif>

<!--- Stored Procs --->
<cfif bFoundStoredProc>
	<br><br>
	<h3>Stored Procs (#qStoredProcs.RecordCount#)</h3>

	<cftry>
		<p class="cfdebug">
			<table border="1" BORDERCOLOR="##7E695D" cellpadding="2" cellspacing="0" class="cfdebug" frame="box">
				<tr>
					<td class="cfdebug_td_heading" align="center"></td>
					<td class="cfdebug_td_heading" align="center">Stored Proc</td>
					<td class="cfdebug_td_heading" align="center">Datasource</td>
					<td class="cfdebug_td_heading">Time</td>
					<td class="cfdebug_td_heading">Cost</td>
					<td class="cfdebug_td_heading">Template</td>
					<td class="cfdebug_td_heading">Line</td>
					<td class="cfdebug_td_heading">Parameters</td>
					<td class="cfdebug_td_heading">Result Sets</td>
				</tr>
				<cfloop query="qStoredProcs">
					<cfif Max(qStoredProcs.executionTime, 0) GTE ExecTimeSlow>
						<cfset ExecTimeClass = 'query_slow'>
					<cfelseif Max(qStoredProcs.executionTime, 0) GTE ExecTimeWarn>
						<cfset ExecTimeClass = 'query_warn'>
					<cfelseif Val(qStoredProcs.cachedquery)>
						<cfset ExecTimeClass = 'query_cached'>
					<cfelse>
						<cfset ExecTimeClass = ''>
					</cfif>

					<cfset QueryCost = (Max(qStoredProcs.executionTime, 0) / qSPSummary.TotalExecutionTime) * 100>

					<tr class="<cfif qStoredProcs.currentRow mod 2>rowLight<cfelse>rowDark</cfif> #ExecTimeClass#">
						<td align="right" class="cfdebug_td_data" nowrap>#qStoredProcs.CurrentRow#</td>
						<td align="right" class="cfdebug_td_data queryname" nowrap>#qStoredProcs.name#</td>
						<td align="right" class="cfdebug_td_data" nowrap>#qStoredProcs.datasource#</td>
						<td align="left" class="cfdebug_td_data executiontime" nowrap>#ExecutionTimeFormat(Max(qStoredProcs.executionTime, 0))# <CFIF qStoredProcs.cachedquery>Cached Query</CFIF></td>
						<td align="left" class="cfdebug_td_data" nowrap>#NumberFormat(QueryCost, '0')#%</td>
						<td align="right" class="cfdebug_td_data" nowrap>#qStoredProcs.Template#</td>
						<td align="right" class="cfdebug_td_data" nowrap>#qStoredProcs.Line#</td>
						<TD CLASS="cfdebug_td_data" nowrap>
							<table border=0 cellpadding=0 cellspacing=0>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>
											<table border=1 cellpadding=2 cellspacing=2 class="parameters">
											<tr><th colspan="5" align="center">parameters</th></tr>
											<tr>
												<td><code><i>type</i></code></td>
												<td><code><i>CFSQLType</i></code></td>
												<td><code><i>variable</i></code></td>
												<td><code><i>dbVarName</i></code></td>
												<td><code><i>value</i></code></td>
											</tr>
											<cfloop index="x" from=1 to="#arrayLen(qStoredProcs.attributes)#">
												<cfset thisParam = #qStoredProcs.attributes[qStoredProcs.currentRow][x]#>

												<tr>
														<td>&nbsp;<code><cfif StructKeyExists(thisParam, "type")>#thisParam.type#</cfif></code></td>
														<td>&nbsp;<code><cfif StructKeyExists(thisParam, "sqlType")>#thisParam.sqlType#</cfif></code></td>
														<td>&nbsp;<code><cfif StructKeyExists(thisParam, "variable")>#thisParam.variable#</cfif></code></td>
														<td>&nbsp;<code><cfif StructKeyExists(thisParam, "dbVarName")>#thisParam.dbVarName#</cfif></code></td>
														<td>&nbsp;<code><cfif StructKeyExists(thisParam, "value")>#thisParam.value#</cfif></code></td>
												</tr>
											</cfloop>
											</table>
									</td>
								</tr>
							</table>
						</td>
						<TD CLASS="cfdebug_td_data" nowrap>
							<table border=0 cellpadding=0 cellspacing=0>
								<tr>
									<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td>
										<table border="1" cellpadding="2" cellspacing="2" class="parameters">
											<tr><th colspan="2" align="center">results</th></tr>
											<tr><td><code><i>name</i></code></td><td><code><i>resultset</i></code></td></tr>

											<cfloop index="x" from=1 to="#arrayLen(qStoredProcs.result)#">
											<cfset thisParam = #qStoredProcs.result[qStoredProcs.currentRow][x]#>
											<tr>
													<td>&nbsp;<code><cfif StructKeyExists(thisParam, "name")>#thisParam.name#</cfif></code></td>
													<td>&nbsp;<code><cfif StructKeyExists(thisParam, "resultSet")>#thisParam.resultSet#</cfif></code></td>
											</tr>
											</cfloop>
											</table>
									</td>
								</tr>
							</table>
						</TD>
					</tr>
				</cfloop>
			</table>
		</p>

		<cfcatch type="Any">
			<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
			<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
		</cfcatch>
	</cftry>
</cfif>

<!--- SCOPE VARIABLES --->
<cfif bFoundScopeVars>
<cftry>
<cfif IsDefined("APPLICATION") AND IsStruct(APPLICATION) AND StructCount(APPLICATION) GT 0 AND cfdebugger.check("ApplicationVar")>

	<br><br>

	<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
	<TR>
		<TD COLSPAN="2" CLASS="ScopeHeading">Application Variables</TD>
	</TR>
	<CFLOOP COLLECTION="#APPLICATION#" item="param">
	<TR>
		<TD CLASS="ScopeVariableName">#param#</TD>
		<TD CLASS="ScopeVariableValue">#CFDebugSerializable(APPLICATION[param])#</TD>
	</TR>
	</CFLOOP>
	</TABLE>
</cfif>

	<cfcatch type="Any">
		<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
		<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
	</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("CGI") AND IsStruct(CGI) AND StructCount(CGI) GT 0 AND cfdebugger.check("CGIVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">CGI Variables</TD>
</TR>
<CFLOOP COLLECTION="#CGI#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(CGI[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("CLIENT") AND IsStruct(CLIENT) AND StructCount(CLIENT) GT 0 AND cfdebugger.check("ClientVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Client Variables</TD>
</TR>
<CFLOOP COLLECTION="#CLIENT#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(CLIENT[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("COOKIE") AND IsStruct(COOKIE) AND StructCount(COOKIE) GT 0 AND cfdebugger.check("CookieVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Cookie Variables</TD>
</TR>
<CFLOOP COLLECTION="#Cookie#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(Cookie[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("FORM") AND IsStruct(FORM) AND StructCount(FORM) GT 0 AND cfdebugger.check("FormVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Form Variables</TD>
</TR>
<CFLOOP COLLECTION="#Form#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(Form[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("REQUEST") AND IsStruct(REQUEST) AND StructCount(REQUEST) GT 0 AND cfdebugger.check("RequestVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Request Variables</TD>
</TR>
<CFLOOP COLLECTION="#Request#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(REQUEST[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("SERVER") AND IsStruct(SERVER) AND StructCount(SERVER) GT 0 AND cfdebugger.check("ServerVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Server Variables</TD>
</TR>
<CFLOOP COLLECTION="#Server#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(Server[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("SESSION") AND IsStruct(SESSION) AND StructCount(SESSION) GT 0 AND cfdebugger.check("SessionVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">Session Variables</TD>
</TR>
<CFLOOP COLLECTION="#Session#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(Session[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

<cftry>
<cfif IsDefined("URL") AND IsStruct(URL) AND StructCount(URL) GT 0 AND cfdebugger.check("URLVar")>

<BR>
<BR>

<TABLE BORDER="1" BORDERCOLOR="##000000" CELLPADDING="4" CELLSPACING="0" FRAME="box">
<TR>
	<TD COLSPAN="2" CLASS="ScopeHeading">URL Variables</TD>
</TR>
<CFLOOP COLLECTION="#URL#" item="param">
<TR>
	<TD CLASS="ScopeVariableName">#param#</TD>
	<TD CLASS="ScopeVariableValue">#CFDebugSerializable(URL[param])#</TD>
</TR>
</CFLOOP>
</TABLE>
</cfif>
<cfcatch type="Any">
	<cflog file="AdeptDebugTemplate" type="error" text="#cfcatch.message#">
	<cflog file="AdeptDebugTemplate" type="error" text="#SerializeJson(cfcatch)#">
</cfcatch>
</cftry>

</cfif>

<BR>
<BR>

<cfset duration = getTickCount() - startTime>
<font size="-1" class="cfdebug"><i>Debug Rendering Time: #duration# ms</i></font><br />

<BR>
<BR>

</cfoutput>
<cfsetting enablecfoutputonly="No">
</cfif>

	</TD>
</TR>
</TABLE>
</div>

<cfscript>
//UDF - Handle output of complex data types.
function CFDebugSerializable(variable)
{
var ret = "";
try
    {
		if(IsSimpleValue(variable))
		{
			ret = variable;
		}
		else
		{
			if (IsStruct(variable))
			{
				ret = ("Struct (" & StructCount(variable) & ")");
			}
			else if(IsArray(variable))
			{
				ret = ("Array (" & ArrayLen(variable) & ")");
			}
			else if(IsQuery(variable))
			{
				ret = ("Query (" & variable.RecordCount & ")");
			}
			else
			{
				ret = ("Complex type");
			}
		}
}
    catch("any" ex)
    {
        ret = "undefined";
    }
    return ret;
}
// UDF - tree writing
function drawNode(nTree, indent, id, highlightThreshold) {
    var templateOuput = "";
    if( nTree[id].duration GT highlightThreshold ) {
        templateOutput = "<font color='red'><span class='template_slow'>(#nTree[id].duration#ms) " & nTree[id].template & " @ line " & #nTree[id].line# & "</span></font><br>";
    } else {
        templateOutput = "<span class='template'>(#nTree[id].duration#ms) " & nTree[id].template & " @ line " & #nTree[id].line# & "</span><br>";
    }
    writeOutput(repeatString("&nbsp;&nbsp;&middot;", indent + 1) & " <img src='http://64.251.200.29/CFIDE/debug/images/arrow.gif' alt='arrow' border='0'><img src='http://64.251.200.29/CFIDE/debug/images/endDoc.gif' alt='top level' border='0'> " & templateOutput);
    return "";
}

function drawTree(tree, indent, id, highlightThreshold) {
    var alength = 1;
    var i = 1;
    var templateOuput = "";

    // top level nodes (application.cfm,cgi.script_name,etc) have a -1 parent line number
    if(tree[id].line EQ -1) {
        writeoutput( "<img src='http://64.251.200.29/CFIDE/debug/images/topdoc.gif' alt='top level' border='0'> " & "<span class='template'><b>(#Tree[id].duration#ms) " & Tree[id].template & "</b></span><br>" );
    } else {
        if( Tree[id].duration GT highlightThreshold ) {
            templateOutput = "<font color='red'><span class='template_slow'>(#Tree[id].duration#ms) " & Tree[id].template & " @ line " & #Tree[id].line# & "</span></font><br>";
        } else {
            templateOutput = "<span class='template'>(#Tree[id].duration#ms) " & Tree[id].template & " @ line " & #Tree[id].line# & "</span><br>";
        }
        writeoutput( repeatString("&nbsp;&nbsp;&middot;", indent + 1) & " <img src='http://64.251.200.29/CFIDE/debug/images/arrow.gif' alt='arrow' border='0'><img src='http://64.251.200.29/CFIDE/debug/images/parentDoc.gif' alt='top level' border='0'> " & templateOutput );
    }

    if( isArray( tree[id].children ) and arrayLen( tree[id].children ) ) {
        alength = arrayLen( tree[id].children );
        for( i = 1; i lte alength; i = i + 1 ) {
            if( isArray(tree[id].children[i].children) and arrayLen( tree[id].children[i].children ) gt 0 ) {
                drawTree(tree, indent + 1, tree[id].children[i].templateid, highlightThreshold);
            } else {
                drawNode(tree, indent + 1, tree[id].children[i].templateid, highlightThreshold);
            }
        }
    } else {
        // single template, no includes?
        //drawNode(tree, indent + 1, tree[id].template, highlightThreshold);
    }
    return "";
}

function ExecutionTimeFormat(iMS)
{
	var returnString = "";

	if (Len(iMS) AND IsNumeric(iMS))
	{
		if (iMS LT 1000) { returnString = iMS & 'ms'; }
		else {
			returnString = NumberFormat(ims/1000, ',0.00') & 's';
		}
	}

	return returnString;
}
</cfscript>
